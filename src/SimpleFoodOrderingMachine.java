import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Objects;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JPanel itemMenu;
    private JButton hamburgerButton;
    private JButton pizzaButton;
    private JButton hotDogButton;
    private JButton juiceButton;
    private JButton iceCreamButton;
    private JButton cakeButton;
    private JLabel orderedItemsLabel;
    private JTextPane orderedItemsList;
    private JLabel totalPriceLabel;
    private JLabel totalPrice;
    private JLabel currencyUnit;
    private JButton checkOutButton;

    public SimpleFoodOrderingMachine() {
        hamburgerButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("icons/hamburger.png"))));
        hamburgerButton.addActionListener(e -> order("Hamburger", 500));
        hamburgerButton.setMargin(new java.awt.Insets(0,0,0,0));
        hamburgerButton.getAccessibleContext().setAccessibleName("Hamburger (500 yen).");
        hamburgerButton.getAccessibleContext().setAccessibleDescription("Press the space key to order.");
        hamburgerButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        pizzaButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("icons/pizza.png"))));
        pizzaButton.addActionListener(e -> order("Pizza", 2000));
        pizzaButton.setMargin(new java.awt.Insets(0,0,0,0));
        pizzaButton.getAccessibleContext().setAccessibleName("Pizza (2000 yen).");
        pizzaButton.getAccessibleContext().setAccessibleDescription("Press the space key to order.");
        pizzaButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        hotDogButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("icons/hot-dog.png"))));
        hotDogButton.addActionListener(e -> order("Hot dog", 400));
        hotDogButton.setMargin(new java.awt.Insets(0,0,0,0));
        hotDogButton.getAccessibleContext().setAccessibleName("Hot dog (400 yen).");
        hotDogButton.getAccessibleContext().setAccessibleDescription("Press the space key to order.");
        hotDogButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        juiceButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("icons/juice.png"))));
        juiceButton.addActionListener(e -> order("Juice", 200));
        juiceButton.setMargin(new java.awt.Insets(0,0,0,0));
        juiceButton.getAccessibleContext().setAccessibleName("Juice (200 yen).");
        juiceButton.getAccessibleContext().setAccessibleDescription("Press the space key to order.");
        juiceButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        iceCreamButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("icons/ice-cream.png"))));
        iceCreamButton.addActionListener(e -> order("Ice cream", 300));
        iceCreamButton.setMargin(new java.awt.Insets(0,0,0,0));
        iceCreamButton.getAccessibleContext().setAccessibleName("Ice cream (300 yen).");
        iceCreamButton.getAccessibleContext().setAccessibleDescription("Press the space key to order.");
        iceCreamButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        cakeButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("icons/cake.png"))));
        cakeButton.addActionListener(e -> order("Cake", 600));
        cakeButton.setMargin(new java.awt.Insets(0,0,0,0));
        cakeButton.getAccessibleContext().setAccessibleName("Cake (600 yen).");
        cakeButton.getAccessibleContext().setAccessibleDescription("Press the space key to order.");
        cakeButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        orderedItemsList.getAccessibleContext().setAccessibleName("Nothing is ordered.");
        orderedItemsList.getAccessibleContext().setAccessibleDescription("");
        orderedItemsList.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });

        checkOutButton.addActionListener(e -> checkOut());
        checkOutButton.getAccessibleContext().setAccessibleName("check out.");
        checkOutButton.getAccessibleContext().setAccessibleDescription("Press the space key to check out.");
        checkOutButton.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background").brighter());
            }

            @Override
            public void focusLost(FocusEvent e) {
                e.getComponent().setBackground(UIManager.getColor("Button.background"));
            }
        });
}

    void order(String itemName, int itemPrice){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + itemName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + itemName + "! It will be served as soon as possible.");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + itemName + " " + itemPrice + " yen" + "\n");
            orderedItemsList.getAccessibleContext().setAccessibleName(currentText + itemName + " " + itemPrice + " yen.");
            orderedItemsList.getAccessibleContext().setAccessibleDescription("");

            int currentTotalPrice = Integer.parseInt(totalPrice.getText());
            totalPrice.setText(String.valueOf(currentTotalPrice + itemPrice));
        }
    }

    void checkOut(){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to check out?",
                "Check Out Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            int currentTotalPrice = Integer.parseInt(totalPrice.getText());
            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + currentTotalPrice + " yen.");
            orderedItemsList.setText("");
            orderedItemsList.getAccessibleContext().setAccessibleName("Nothing is ordered.");
            orderedItemsList.getAccessibleContext().setAccessibleDescription("");

            totalPrice.setText("0");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
